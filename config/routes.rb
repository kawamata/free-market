Rails.application.routes.draw do


  namespace :users do

    resources :products
# お気に入り商品ページ
    get 'user_likes', to: 'products#user_likes', as: :user_likes
    # お気に入り登録
    get 'user_like/:id', to: 'products#user_like', as: :user_like

# ユーザーページ（トップ）
    # プロフィール
    get 'profile/:id', to: 'users#show', as: :users_profile
    # プロフィール更新
    get 'profile/:id/edit', to: 'users#edit', as: :edit_profile
    patch 'profile/:id', to: 'users#update'

# ログイン機構
    # ユーザー登録
    get 'sign_up', to: 'users#sign_up'
    post 'sign_up', to: 'users#sign_up_process'
    # ログイン
    get 'sign_in', to: 'users#sign_in'
    post 'sign_in', to: 'users#sign_in_process'
    # ログアウト
    delete 'sign_out', to: 'users#sign_out'
  end


# 商品一覧（トップ）
  root 'markets/markets#index'

# 一覧ページ
  namespace :markets do
    # 以下、検索と詳細の順番を逆にすると、検索時にshowアクションが読まれる
    # 商品検索
    get 'search', to: 'markets#search', as: :search
    # 商品詳細
    get '/:id', to: 'markets#show', as: :product_show

    get '/:id/purchase', to: 'markets#purchase', as: :purchase
    # 購入する
    post '/:id/buy', to: 'markets#buy', as: :buy
  end

end
