# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


# Category.create([{ name: '日用品' }, { name: 'アウトドア' }, { name: 'ペット用品' }])

# Product.create({name: "小人用孫の手", description: "孫の代わりに", price: 350, category_id: 4}, 
# 	{name: "シャンプー", description: "オーガニック", price: 1200, category_id: 1}, 
# 	{name: "孫の手", description: "孫の代わりに", price: 500, category_id: 1})