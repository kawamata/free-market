module ProductsHelper

# ダミー画像設定
# 一括で小さいサイズにリサイズして、大きく見せたい場所ではサイズ指定して引き延ばす
	def image_url_1(product)
		if product.image1.blank?
			"https://dummyimage.com/200x200/000/fff"
		else
			product.image1.thum50.url
		end
	end
	def image_url_2(product)
		if product.image2.blank?
			"https://dummyimage.com/200x200/000/fff"
		else
			product.image2.thum50.url
		end
	end
	def image_url_3(product)
		if product.image3.blank?
			"https://dummyimage.com/200x200/000/fff"
		else
			product.image3.thum50.url
		end
	end

end
