class Product < ApplicationRecord
	belongs_to :user
	belongs_to :category
	has_many :user_likes

	# サムネイルの設定（paperclip)
	# 参考：https://qiita.com/chamao/items/181451a0f6d875a3c407
	# has_attached_file :image1,
 #                      styles: { medium: "300x300>", thumb: "100x100>" },
 #                      path: "#{Rails.root}/app/assets/images/products/:filename"
 #  	validates_attachment_content_type :image1, content_type: /\Aimage\/.*\z/


 	# carrierwave
  	mount_uploader :image1, ImageUploader
  	mount_uploader :image2, ImageUploader
  	mount_uploader :image3, ImageUploader


# 名前検索
  	scope :name_like, -> key_word { where('name like ?', "%#{key_word}%")}
# カテゴリー検索
	# scope :by_category, -> category_id { joins(:category).where(category_id: category_id)}
	scope :by_category, -> category_id { where(category_id: category_id)}
# 価格検索（scope使う場合）
	scope :price_range, ->(range) { where(price: range) }


# ----------------------------------------------------
# statusカラムの値の触り方（初期値を入れない場合）
	# 出品中か売り切れかの判断
	# enum status: {
	# 	on_sale:  0,
	# 	sold: 1
	# }
	# # 商品登録時に statusを'0'にする
	# before_create { self.status = Product.statuses[:on_sale] }
# ----------------------------------------------------


  	def like_by?(user)
  		self.user_likes.exists?(user_id: user.id)
  	end

end


# :image1, :image2, :image3,



# joinsはモデルにしか使えない　一度変数に格納すると、使えないので
# まずモデルをjoinsさせる
# Product.joins(:category).where(@products.pluck(:id))
