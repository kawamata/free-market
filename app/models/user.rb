class User < ApplicationRecord
	# include Paperclip::Glue

	has_many :products
	has_many :user_likes

	# passwordとpassword_confirmationカラムが存在しなくても、存在するかのように振る舞う
	# gem 'bcrypt' が必要
	has_secure_password validations: true
	# 明示的に、presenceと最低文字数を設定（password）
	validates :password, presence: true, length: { minimum: 4 }
	validates :name, presence: true

	# emailの形式制限
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true, format: {with: VALID_EMAIL_REGEX}, uniqueness: true

	# サムネイルの設定（paperclip)
	# 参考：https://qiita.com/chamao/items/181451a0f6d875a3c407
	has_attached_file :image,
                      styles: { medium: "300x300>", thumb: "100x100>" },
                      path: "#{Rails.root}/app/assets/images/:filename"
  	validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

end
