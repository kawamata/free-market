class Markets::MarketsController < ApplicationController
	layout 'layouts/2nd_application'
  	def index
  		@products = Product.all.order(updated_at: :desc)
  	end

	def show
		@product = Product.find(params[:id])
	end

	def search
		@products = Product.all

# scopeとは、SQL文をメソッド化するもの
# -------------------------------------------
	# キーワード検索
		@products = @products.name_like(params[:key_word]) if params[:key_word].present?
	# カテゴリー検索
		@products = @products.by_category(params[:category_id]) if params[:category_id].present?
# -------------------------------------------
	# 価格検索
		# 以下は値のどちらかが空の場合にエラーを返す
		# if params[:price_low].present? && params[:price_high].present?
		# 	@products = @products.price_range(params[:price_low]..params[:price_high])
		# elsif params[:price_low].present? || params[:price_high].present?
		# 	flash.now[:warning] = '価格検索のどちらかの値が空欄です'
		# end

		# 価格検索もうひとつの書き方（scope使用）- 空の要素に Float::INFINITY使うと、Infinityになる（この場合は終わりの値が空の時、プラス値の無限数）
		# 最初の値が空""の場合、to_iで数値化することで、0に置き換わるのでどちらの値が空欄でも検索可能（0..Infinity)
		@products = @products.price_range(params[:price_low].to_i..(params[:price_high].blank? ? Float::INFINITY : params[:price_high].to_i))
# -------------------------------------------
	# 並び替え
		@products = @products.order(price: :desc) if params[:sort] === "high"
		@products = @products.order(price: :asc) if params[:sort] === "low"
		@products = @products.order(updated_at: :asc) if params[:sort] === "old"
		@products = @products.order(updated_at: :desc) if params[:sort] === "new"

		# 商品一覧で検索結果を表示
		render 'markets/markets/index' 
		# render とredirect_to の違い = renderはアクション内で呼ぶviewを指定。redirect_toは、HTTPリクエストをサーバーに送り、そこから返る結果をhtmlに表示。
	end


	def purchase
		@product = Product.find(params[:id])
	end

	def buy
		# enumを使う場合
		# Product.find(params[:id]).sold!
		Product.find(params[:id]).update(status: 1)
		redirect_to root_path, flash: { success: '商品を購入しました' }
	end

end


# カテゴリーで一度抽出した要素から、
# キーワードで抽出した要素を検索する
# @products.where(category_id: 1)
