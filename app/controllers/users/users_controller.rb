class Users::UsersController < ApplicationController

	# ログイン時に、ログインユーザーを特定する必要あるので、set_userでユーザー特定をまず行う
	before_action :set_user, only: :sign_in_process

	def show
		@user = User.find(params[:id])
	end

	def edit
		@user = User.find(current_user.id)
	end

	def update
		if current_user.update(user_params)
			flash[:success] = '更新しました。'
			redirect_to users_users_profile_url
		else
			render 'edit'
		end
	end

	def sign_up
		@user = User.new
		render layout: '2nd_application'
	end

	def sign_up_process
		user = User.new(user_params)
		if user.save
			flash[:success] = '登録しました。'
			user_sign_in(user)
			redirect_to root_path
		else
			flash[:danger] = '登録に失敗しました。やり直してください。'
			flash[:warning] = user.errors.full_messages[0]
			redirect_to users_sign_up_url(user)
		end
	end

	def sign_in
		@user = User.new
		render layout: '2nd_application'
	end

	def sign_in_process
		if user_params[:email].present? && user_params[:password].present?
			if @user.authenticate(user_params[:password]) && @user.email == user_params[:email]
				user_sign_in(@user)
				flash[:success] = 'ログインしました。'
				redirect_to root_path
			else
				flash[:danger] = 'ログインに失敗しました'
				render 'sign_in'
			end
		else
			flash[:danger] = "必要項目を入力してください"
			redirect_to users_sign_in_path
		end
	end

	def sign_out
		user_sign_out
		flash[:success] = 'ログアウトしました。'
		redirect_to users_sign_in_url
	end

	# def authorize
	# end

	private
		def user_params
			params.require(:user).permit(:name, :email, :password, :password_confirmation, :profile, :image)
		end

		def set_user
			@user = User.find_by!(email: user_params[:email]) if user_params[:email].present?
		end


end
