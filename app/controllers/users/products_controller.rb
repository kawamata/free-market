class Users::ProductsController < ApplicationController

	def index
		# binding.pry
		# @user = User.find(params[:id])
		# @products = User.find(params[:id]).products.all
		@products = Product.where(user_id: current_user)
	end

	# def show
	# 	@product = Product.find(params[:product_id])
	# end

	def show
		@product = Product.find_by(id: params[:id])
	end

	def new
		@product = Product.new
	end

	def create
		product = Product.new(product_params)
		if product.save
			redirect_to root_url, flash: { success: '商品を登録しました。' }
		else
			flash[:danger] = '商品登録に失敗しました。'
			render 'new'
		end
	end

	def edit
		@product = Product.find(params[:id])
	end

	def update
		@product = Product.find(params[:id])
		@product.update(product_params)
		flash[:suceess] = "商品を編集しました。"
		redirect_to users_product_url
	end

	def destroy
		@product = Product.find(params[:id])
		@product.destroy
		flash[:success] = '商品を削除しました。'
		redirect_to users_products_url
	end

	def user_like
		if UserLike.exists?(user_id: current_user.id, product_id: params[:id])
			UserLike.find_by(user_id: current_user.id, product_id: params[:id]).destroy
			redirect_back(fallback_location: root_path)
		else
			UserLike.create(user_id: current_user.id, product_id: params[:id])
			redirect_back(fallback_location: root_path)
		end
	end

	def user_likes
		product_ids = User.find(current_user.id).user_likes.pluck(:product_id)
		@products = product_ids.map { |id| Product.find(id) }		
	end

	private

	def product_params
		params.require(:product).permit(:name, :description, :price, :category_id, :user_id, :image1, :image2, :image3)
	end
end
