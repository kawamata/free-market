class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include UsersHelper
  
  # bootstrapでflash装飾
  # add_flash_types :success, :info, :warning, :danger
end
